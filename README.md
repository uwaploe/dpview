# Deep Profiler Status Viewer

Dpview provides a simple TUI (Text User Interface) for monitoring the status of a Deep Profiler from the Docking Station Controller (DSC).

There are two display screens, one for the engineering data and one for the science data. At the top of each display box is the date and time that the displayed data values were received. A question mark is displayed as a value if the value is determined to be "out of range". The *n* (next) and *p* (previous) keys are used to navigate between the screens, press *q* to quit.


## Engineering Data

![img](./EngineeringData.png)

### DSC Data

| **Variable**    | **Description**                                                    |
|--------------- |------------------------------------------------------------------ |
| T_amb | Ambient temperature inside pressure housing                        |
| T_hs  | Heat-sink temperature                                              |
| RH              | Relative humidity                                                  |
| IPS stat        | IPS controller status bits, most significant bit first (see below) |
| 12v cur         | 12-volt power supply current draw                                  |

| **IPS Status Bit** | **Description**                         |
|------------------ |--------------------------------------- |
| 0                  | IPS bias enabled (active high)          |
| 1                  | IPS charging enabled (active high)      |
| 2                  | Unused                                  |
| 3                  | IPS charging active (active low)        |
| 4                  | IPS charging current >= 5A (active low) |
| 5                  | Unused                                  |
| 6                  | Unused                                  |
| 7                  | Unused                                  |


### DPC Data

| **Variable**     | **Description**                                    |
|---------------- |-------------------------------------------------- |
| Profile#         | The current profile number                         |
| State            | Profiler state                                     |
| Pressure         | Pressure measured by the McLane controller (MPC)   |
| Internal temp    | Pressure housing / Battery Sphere temperature      |
| Vmon             | Battery voltage measure at voltage monitor circuit |
| Battery Voltage  | Voltage reported by battery I2C controller         |
| Battery Current  | Current reported by battery I2C controller         |
| Battery Capacity | Battery "fuel gauge" estimate                      |
| Motor Current    | Motor current measured by MPC                      |


## Science Data

This screen shows the most recent data from all of the DPC sensors.

![img](./ScienceData.png)
