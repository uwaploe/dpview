// TUI to view real-time Deep Profiler data on the DSC.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"

	"bitbucket.org/uwaploe/go-dputil"
	"github.com/garyburd/redigo/redis"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

type Orientation int

const (
	DT_FORMAT              = "2006-01-02 15:04:05"
	HORIZONTAL Orientation = 0
	VERTICAL   Orientation = 1
)

var Version = "dev"
var BuildDate = "unknown"

// Goroutine to subscribe to the science and engineering data records
// and pass them to the main thread as custom UI events.
func dataReader(psc redis.PubSubConn) <-chan redis.Message {
	ch := make(chan redis.Message)
	psc.Subscribe("data.sci", "data.eng")
	go func() {
		defer close(ch)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				ch <- msg
			}
		}
	}()

	return ch
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] \n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Real-time display of DP data records\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	server := flag.String("server", "localhost:6379",
		"network address (HOST:PORT) of Redis server")

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Build date: %s\n", BuildDate)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	conn, err := redis.Dial("tcp", *server)

	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	err = ui.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer ui.Close()

	termWidth, termHeight := ui.TerminalDimensions()

	help := widgets.NewParagraph()
	help.Text = fmt.Sprintf("q=quit n=next p=prev  (%s)", Version)
	help.TextStyle.Fg = ui.ColorYellow
	help.BorderStyle.Fg = ui.ColorYellow
	help.Border = true
	help.Title = "Help"

	// Create all of the UI tables
	for _, v := range dataDisplay {
		v.create_table()
	}

	// Layout the engineering data page (tab)
	page1 := ui.NewGrid()
	page1.SetRect(0, 0, termWidth, termHeight)
	page1.Set(
		ui.NewCol(1.0/2,
			ui.NewRow(7.0/8, dataDisplay["dock"].table),
			ui.NewRow(1.0/8, help)),
		ui.NewCol(1.0/2,
			ui.NewRow(1.0, dataDisplay["profiler"].table)),
	)

	// Layout the science data page
	page2 := ui.NewGrid()
	page2.SetRect(0, 0, termWidth, termHeight)
	page2.Set(
		ui.NewCol(0.4,
			ui.NewRow(7.0/8, dataDisplay["acm_1"].table),
			ui.NewRow(1.0/8, help)),
		ui.NewCol(0.6,
			ui.NewRow(7.0/24, dataDisplay["ctd_1"].table),
			ui.NewRow(5.0/24, dataDisplay["optode_1"].table),
			ui.NewRow(5.0/24, dataDisplay["flntu_1"].table),
			ui.NewRow(3.0/24, dataDisplay["flcd_1"].table)),
	)

	pane := widgets.NewTabPane("data.eng", "data.sci")
	pane.SetRect(0, 0, termWidth, 3)
	pane.Border = true

	renderTab := func() {
		switch pane.ActiveTabIndex {
		case 0:
			ui.Render(page1)
		case 1:
			ui.Render(page2)
		}
	}

	ui.Render(help, pane, page1)

	ch := dataReader(psc)
	uiEvents := ui.PollEvents()

	for {
		select {
		case msg := <-ch:
			dr := dputil.DataRecord{}
			if err := json.Unmarshal(msg.Data, &dr); err == nil {
				if entry, ok := dataDisplay[dr.Source]; ok {
					if rec, ok := dr.Data.(map[string]interface{}); ok {
						entry.update(rec, dr.T)
						renderTab()
					}
				}
			}
		case e := <-uiEvents:
			switch e.ID {
			case "q", "<C-c>":
				psc.Unsubscribe()
				return
			case "n", "<Right>":
				pane.FocusRight()
				ui.Clear()
				ui.Render(help, pane)
				renderTab()
			case "p", "<Left>":
				pane.FocusLeft()
				ui.Clear()
				ui.Render(help, pane)
				renderTab()
			}
		}
	}

}
