#!/usr/bin/env bash

BINDIR="$HOME/bin"
SVCDIR="$HOME/sv"
PROGDIR="$(pwd)"

set -e
reqs=()
for req in "${reqs[@]}"; do
    type "$req"
done

# Install programs
files=(*)
for f in "${files[@]}"; do
    # skip directories
    [[ -d $f ]] && continue
    # skip non-executable files
    [[ -x $f ]] || continue
    ln -v -s -f "$PROGDIR/$f" "$BINDIR"
done

# Install runit service directory trees
if [[ -d runit ]]; then
    files=(runit/*)
    for f in "${files[@]}"; do
        # Only copy directories
        [[ -d $f ]] || continue
        cp -av "$f" $SVCDIR
    done
fi

# Install any additional executable scripts
if [[ -d bin ]]; then
    files=(bin/*)
    for f in "${files[@]}"; do
        # skip directories
        [[ -d $f ]] && continue
        # skip non-executable files
        [[ -x $f ]] || continue
        cp -av "$f" "$BINDIR"
    done
fi
