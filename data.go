// Data definitions for dpview
package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// A DataEntry defines how to display a data record field
type DataEntry struct {
	title     string
	key       string
	formatter func(e *DataEntry, t time.Time, v interface{}) string
	lastVal   interface{}
	lastT     time.Time
}

func (e *DataEntry) format(rec map[string]interface{}, ts time.Time) string {
	val, ok := rec[e.key]
	if !ok {
		return "[   n/a  ](fg-red)"
	}
	return e.formatter(e, ts, val)
}

// A DataTable defines the display for a data record
type DataTable struct {
	title string
	style Orientation
	data  []*DataEntry
	table *widgets.Table
}

// Map data record names to DataTable's
var dataDisplay map[string]*DataTable = map[string]*DataTable{
	"dock": {
		title: "DSC Status",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "T_amb (\xc2\xb0C)",
				key:   "T_ambient",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%5.1f", x*0.001)
					} else {
						return "?"
					}
				},
			},
			{
				title: "T_hs (\xc2\xb0C)",
				key:   "T_heatsink",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%5.1f", x*0.001)
					} else {
						return "?"
					}
				},
			},
			{
				title: "RH (%)",
				key:   "humidity",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					val, ok := v.(float64)
					if !ok || val <= 0 {
						return "?"
					} else {
						return strconv.FormatInt(int64(val*0.1), 10)
					}
				},
			},
			{
				title: "IPS stat",
				key:   "ips_status",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%08b", int64(x)&0xff)
					} else {
						return "?"
					}
				},
			},
			{
				title: "12v cur. (A)",
				key:   "v12",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%5.1f", x*0.001)
					} else {
						return "?"
					}
				},
			},
		},
	},
	"profiler": {
		title: "DPC Status",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Profile#",
				key:   "profile",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if p, ok := v.(float64); ok {
						return strconv.FormatInt(int64(p), 10)
					} else {
						return "?"
					}
				},
			},
			{
				title: "State",
				key:   "state",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if s, ok := v.(string); ok {
						return s
					} else {
						return "?"
					}
				},
			},
			{
				title: "Pressure (dbar)",
				key:   "pressure",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok {
						return "?"
					} else {
						return fmt.Sprintf("%7.1f", x*0.001)
					}
				},
			},
			{
				title: "Speed (dbar/s)",
				key:   "pressure",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					p, ok := v.(float64)
					if !ok {
						return "?"
					} else {
						p = p * 0.001
						if e.lastVal == nil {
							e.lastT = t
							e.lastVal = p
							return ""
						}
						dt := t.Sub(e.lastT).Seconds()
						dp := p - e.lastVal.(float64)
						e.lastT = t
						e.lastVal = p
						return fmt.Sprintf("%5.2f", dp/dt)
					}
				},
			},
			{
				title: "Internal Temp. (\xc2\xb0C)",
				key:   "itemp",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					out := make([]string, 0)
					if vals, ok := v.([]interface{}); ok {
						for _, x := range vals {
							out = append(out,
								fmt.Sprintf("%5.1f", x.(float64)*float64(0.1)))
						}
					}
					return strings.Join(out, "/")
				},
			},
			{
				title: "RH (%)",
				key:   "humidity",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok || x <= 0 {
						return "?"
					} else {
						return fmt.Sprintf("%5.1f", x*0.1)
					}
				},
			},
			{
				title: "Vmon (V)",
				key:   "vmon",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok || x <= 0 {
						return "?"
					} else {
						return fmt.Sprintf("%6.2f", x*0.001)
					}
				},
			},
			{
				title: "Battery Voltage (V)",
				key:   "voltage",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok || x >= 17000 || x <= 0. {
						return "?"
					} else {
						return fmt.Sprintf("%6.2f", x*0.001)
					}
				},
			},
			{
				title: "Battery Current (A)",
				key:   "current",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok {
						return "?"
					} else {
						return fmt.Sprintf("%6.3f", x*0.001)
					}
				},
			},
			{
				title: "Battery Capacity (%)",
				key:   "rel_charge",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					val, ok := v.(float64)
					if !ok || val > 100 || val <= 0 {
						return "?"
					} else {
						return strconv.FormatInt(int64(v.(float64)), 10)
					}
				},
			},
			{
				title: "Motor Current (A)",
				key:   "motor_current",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					x, ok := v.(float64)
					if !ok {
						return "?"
					} else {
						return fmt.Sprintf("%6.3f", x*0.001)
					}
				},
			},
		},
	},
	"ctd_1": {
		title: "CTD",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Conductivity (mS/cm)",
				key:   "condwat",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.3f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Temperature (\xc2\xb0C)",
				key:   "tempwat",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.3f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Pressure (dbar)",
				key:   "preswat",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.1f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
		},
	},
	"optode_1": {
		title: "OPTODE",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Cal. Phase (\xc2\xb0)",
				key:   "doconcs",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%8.3f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Temperature (\xc2\xb0C)",
				key:   "t",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%8.3f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
		},
	},
	"acm_1": {
		title: "ACM",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Velocity A (cm/s)",
				key:   "va",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Velocity B (cm/s)",
				key:   "vb",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Velocity C (cm/s)",
				key:   "vc",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Velocity D (cm/s)",
				key:   "vd",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Mag. flux X",
				key:   "hx",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Mag. flux Y",
				key:   "hy",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Mag. flux Z",
				key:   "hz",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Tilt X (\xc2\xb0)",
				key:   "tx",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Tilt Y (\xc2\xb0)",
				key:   "ty",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return fmt.Sprintf("%7.2f", x)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
		},
	},
	"flntu_1": {
		title: "FLNTU",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Chl-a conc. (counts)",
				key:   "chlaflo",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return strconv.FormatInt(int64(x), 10)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
			{
				title: "Turbidity (counts)",
				key:   "ntuflo",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return strconv.FormatInt(int64(x), 10)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
		},
	},
	"flcd_1": {
		title: "FLCD",
		style: VERTICAL,
		data: []*DataEntry{
			{
				title: "Colored Dissolved Org. Matter (counts)",
				key:   "cdomflo",
				formatter: func(e *DataEntry, t time.Time, v interface{}) string {
					if x, ok := v.(float64); ok {
						return strconv.FormatInt(int64(x), 10)
					} else {
						return "[ n/a ](fg-red)"
					}
				},
			},
		},
	},
}

func (dt *DataTable) create_table() {
	n := len(dt.data)
	dt.table = widgets.NewTable()
	dt.table.Border = true
	dt.table.RowSeparator = true
	dt.table.Title = dt.title
	dt.table.TitleStyle.Fg = ui.ColorGreen
	dt.table.BorderStyle.Fg = ui.ColorGreen
	if dt.style == VERTICAL {
		dt.table.Rows = make([][]string, n)
		for i := range dt.table.Rows {
			dt.table.Rows[i] = make([]string, 2)
			dt.table.Rows[i][0] = dt.data[i].title
			dt.table.Rows[i][1] = "[   n/a  ](fg-red)"
		}
	} else {
		dt.table.Rows = make([][]string, 2)
		dt.table.Rows[0] = make([]string, n)
		dt.table.Rows[1] = make([]string, n)
		for i := range dt.data {
			dt.table.Rows[0][i] = dt.data[i].title
			dt.table.Rows[1][i] = "[   n/a  ](fg-red)"
		}
	}

	// min_width := len(dt.title) + len(DT_FORMAT) + 10
}

func (dt *DataTable) update(rec map[string]interface{}, ts time.Time) {
	if dt == nil || dt.table == nil {
		return
	}
	dt.table.Title = fmt.Sprintf("%s %s",
		dt.title,
		ts.Format(DT_FORMAT))
	if dt.style == VERTICAL {
		for i, e := range dt.data {
			dt.table.Rows[i][1] = e.format(rec, ts)
		}
	} else {
		for i, e := range dt.data {
			dt.table.Rows[1][i] = e.format(rec, ts)
		}
	}
}
