module bitbucket.org/uwaploe/dpview

require (
	bitbucket.org/uwaploe/go-dputil v1.3.0
	github.com/garyburd/redigo v1.6.0
	github.com/gizak/termui/v3 v3.0.0
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-wordwrap v1.0.0 // indirect
)

go 1.13
