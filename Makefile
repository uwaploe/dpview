#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

PROG = dpview
SRCS = $(wildcard *.go)
TESTS = $(wildcard *_test.go)

ifdef NATIVE
GOOS = $(shell go env GOOS)
GOARCH = $(shell go env GOARCH)
GOARM =
else
GOOS = linux
GOARCH = arm
GOARM = 5
endif

GOBUILD = CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) GOARM=$(GOARM) go build
BINDIR = build-$(GOOS)_$(GOARCH)

SUBDIRS =
.PHONY: clean $(SUBDIRS) dist vdist

all: $(BINDIR)/$(PROG)

$(SUBDIRS):
	$(MAKE) -C $@

$(BINDIR)/$(PROG): $(patsubst %_test.go,,$(SRCS))
	@mkdir -p $(BINDIR)
	$(GOBUILD) -o $(BINDIR)/$(PROG) \
	  -tags release \
	  -ldflags '-X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'
	ln $(BINDIR)/$(PROG) $(BINDIR)/$(PROG)-$(VERSION)

dist: $(BINDIR)/$(PROG)
	cd $(BINDIR) && upload-to-bitbucket.sh uwaploe $(PROG)

vdist: $(BINDIR)/$(PROG)
	cd $(BINDIR) && \
	  upload-to-bitbucket.sh uwaploe $(PROG)-$(VERSION)

clean:
	rm -rf build-*
	for dir in $(SUBDIRS); do \
	    $(MAKE) -C $$dir clean; \
	done
